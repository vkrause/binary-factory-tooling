#!/usr/bin/python3
import os
import sys
import json
import yaml
import argparse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--projects', type=str, required=True)
arguments = parser.parse_args()

# Grab our list of projects and their configuration
with open(arguments.projects, 'r') as dataFile:
    # Parse the YAML file
    projectsToCreate = yaml.load( dataFile )

# Our output will be a list of Dictionaries, containing several keys:
# 1) The name of the job
# 2) The Craft Blueprints to be built as part of the job
# 3) The Craft Blueprints the job should package
# 4) The description for the resulting job
jobsGathered = []

# We need to let Craft know which target it should be using
platformForCraft = {
    'win64': 'windows-msvc2017_64-cl',
    'mingw64': 'windows-mingw_64-gcc',
    'macos': 'macos-64-clang',
    'appimage': ''
}

# Let's get started!
for project in projectsToCreate.keys():
    # Grab the configuration for the project out to make it easier to access
    projectConfig = projectsToCreate[ project ]

    # For each project, it will have a series of platforms which it is enabled for
    # So we now go over each of those in turn
    for platform in projectConfig['platforms']:
        # Each version will be built on all of the platforms
        for version in projectConfig['versions']:
            # Create a nice little description for this job
            jobDescription = "{0} build for {1} on {2}".format( version['name'], project, platform )

            # Make sure we have a valid system target
            if platform not in platformForCraft:
                continue

            # Determine what blueprints we need to build and make sure what we've been given is valid
            buildBlueprint = projectConfig['buildBlueprint']
            if not isinstance(buildBlueprint, str):
                raise TypeException()

            # Now determine what Blueprints we have to always rebuild
            rebuildBlueprint = projectConfig.get('rebuildBlueprint', [])
            if isinstance(rebuildBlueprint, str):
                rebuildBlueprint = [rebuildBlueprint]

            # setup the options string
            options = version['options'] if 'options' in version else projectConfig.get('options', [])
            if options:
                if isinstance(options, str):
                    options = [options]

            # At this point we are good to go!
            jobEntry = {
                'project': project,
                'platform': platform,
                'craftPlatform': platformForCraft[platform],
                'target': version['target'] or '',
                'targetName': version['name'] or '',
                'packageAppx': version.get('packageAppx', 'False'),# whether to create an experimental Windows store package
                'description': jobDescription,
                'buildBlueprint': buildBlueprint,
                'rebuildBlueprint': ' '.join( [buildBlueprint] + rebuildBlueprint),
                'options': '--options {0}'.format(' '.join(options)) if options else ''
            }

            # Make sure we add it to the list
            jobsGathered.append( jobEntry )

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
    json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)

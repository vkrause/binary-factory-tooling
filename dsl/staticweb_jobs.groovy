// Read the contents of the gathered-jobs.json file a step created for us previously
def jobsToParse = readFileFromWorkspace('staticweb/jobs.json')
def knownJobs = new groovy.json.JsonSlurper().parseText( jobsToParse )

// Iterate over all of the known jobs and create them!
knownJobs.each {
	// Save our job name for later
	def jobName = "Website_${it.name}"
	// Likewise with our cron schedule
	def cronSchedule = "${it.cron}"

	// Read in the necessary Pipeline script
	def pipelineTemplate = readFileFromWorkspace("staticweb/${it.type}.pipeline")

	// Now we can construct our Pipeline script
	// We append a series of variables to the top of it to provide a variety of useful information to the otherwise templated script
	// These appended variables are what makes one build different to the next, aside from the template which was used
	def pipelineScript = """
		|def name = "${it.name}"
		|def deploypath = "${it.deploypath}"
		|def gitUrl = "https://anongit.kde.org/${it.repository}.git"
		|def gitBranch = "${it.branch}"
		|${pipelineTemplate}""".stripMargin()

	// Actually create the job now
	pipelineJob( jobName ) {
		properties {
			// We don't want to keep build results forever
			// We'll set it to keep the last 10 builds and discard everything else
			buildDiscarder {
				strategy {
					logRotator {
						numToKeepStr("5")
						daysToKeepStr('')
						artifactDaysToKeepStr('')
						artifactNumToKeepStr('')
					}
				}
			}
			// We don't want to be building the same project more than once
			// This is to prevent one project hogging resources
			// And also has a practical component as otherwise an older build could finish afterwards and upload old build results
			disableConcurrentBuilds()
		}
		triggers {
			// We want to enable SCM Polling so that git.kde.org can tell Jenkins to look for changes
			// At the same time, we don't want Jenkins scanning for changes, so set the Polling specification to be empty so nothing gets scheduled
			pollSCM {
				scmpoll_spec('')
				ignorePostCommitHooks(false)
			}
			// We want to automatically rebuild once a day
			cron( cronSchedule )
		}
		// This is where the Pipeline script actually happens :)
		definition {
			cps {
				script( pipelineScript )
				sandbox()
			}
		}
	}
}

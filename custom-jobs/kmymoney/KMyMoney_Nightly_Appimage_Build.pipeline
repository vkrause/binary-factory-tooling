// Request a node to be allocated to us
node( "Appimage1404" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// KMyMoney Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'kmymoney/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/kmymoney']]
			]

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'kmymoney-appimage-deps.tar', projectName: 'KMyMoney_Nightly_Appimage_Dependency_Build'

			// Now we unpack them
			sh """
				mkdir -p $HOME/appimage-workspace/
				cd $HOME/appimage-workspace/
				tar -xf $WORKSPACE/kmymoney-appimage-deps.tar
			"""
		}

		// Let's build KMyMoney that we have everything we need
		stage('Building KMyMoney') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				ln -s /usr/bin/gcc-6 $HOME/tools/bin/cc
				ln -s /usr/bin/gcc-6 $HOME/tools/bin/gcc
				ln -s /usr/bin/g++-6 $HOME/tools/bin/c++
				ln -s /usr/bin/g++-6 $HOME/tools/bin/g++

				kmymoney/packaging/linux/appimage/build-kmymoney.sh $HOME/appimage-workspace/ $WORKSPACE/kmymoney/
			"""
		}

		// Now we can generate the actual Appimages!
		stage('Generating KMyMoney Appimage') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				kmymoney/packaging/linux/appimage/build-image.sh $HOME/appimage-workspace/ $WORKSPACE/kmymoney/
				mv $HOME/appimage-workspace/*.AppImage $WORKSPACE/
			"""
		}

		// Finally we capture the appimages for distribution to users
		stage('Capturing Appimages') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.AppImage', onlyIfSuccessful: true
		}
	}
}
}
